#include "myserver.h"

MyServer::MyServer(QObject *parent) : QObject(parent)
{
    server = new QTcpServer(this);
    connect(server,SIGNAL(newConnection()),this,SLOT(newConnection()));

    if (!server->listen(QHostAddress::Any,1234)){

        qDebug()<<"Server not start";
    }
    else {
        qDebug()<<"Server start";
    }
}

void MyServer::newConnection()
{
    qDebug()<<"newConnection";
    QTcpSocket *socket = server->nextPendingConnection();
    connect(socket, &QAbstractSocket::disconnected,
                socket, &QObject::deleteLater);
    socket->write("HTTP/1.1 200 OK\r\n\r\n hello client\r\n");
    socket->disconnectFromHost();
//    socket->flush();
//    socket->waitForBytesWritten(3000);
//    socket->close();
}
/*
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    qDebug()<< socket->readAll();
    QString response = "HTTP/1.1 200 OK\r\n\r\n%1";
    socket->write(response.arg(QTime::currentTime().toString()).toUtf8());
    socket->disconnectFromHost();
*/
